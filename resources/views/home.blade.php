@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
Media Online

@endsection

@section('content')



{{-- Input Postingan --}}
    

    {{-- Modal --}}


<!-- Modal -->

{{-- Akhir Modal --}}

    {{-- Tampilan Semua Postingan Home --}}

    <div class="container justify-content-center">
        <div class="card  mb-3 " >
          <div class="card-header bg-transparent ">
            
            <ul class="nav justify-content-start">
              <li class="nav-item">
                <img src="{{ asset('img/Foto Pratama.jpg') }}" class="rounded-circle" style="height: 40px; width: 40px;" alt="">
              </li>
              <li class="nav-item">
                <a class="nav-link text-dark" href="#">Pratama Adi Ciptawan</a>
              </li>          
            </ul>

          </div>
          <div class="card-body text-black">
            {{-- Komentar Disini --}}
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  
            

            {{-- Like --}}

            <i class="fas fa-thumbs-up font-weight-normal"> 132</i>
            <h5 class="float-right font-weight-normal">12 Komentar</h5>

            {{-- Akhir Like --}}

            <hr class="border-dark" >

            <div class="btn-toolbar justify-content-around" role="toolbar" aria-label="Toolbar with button groups">
              <div class="btn-group" role="group" aria-label="First group">
                <button type="button" class="btn btn-warning font-weight-normal"><i class="far fa-thumbs-up"></i> Like</i></button>
                
              </div>
              <div class="btn-group" role="group" aria-label="Second group">
                <button type="button" class="btn btn-warning" id="komentarPost"><i class="far fa-comment-alt"> Komentar</i></button>
                
              </div>
              
            </div>

            <hr class="border-dark" >
          </div>

          {{-- Komentar user lain --}}
            <div class="container" id="isiKomentar">
            {{-- disini akan muncul komentarnya --}}
            </div>

        </div>
    </div>
  {{-- Akhir Semua Postingan Home --}}

@endsection