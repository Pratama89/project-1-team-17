@extends('layouts/masterProfil')


  @push('style')
  <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('admin/dist/css/adminlte.min.css') }}">
  @endpush

  
@section('content')


<div class="login-box">
  <!-- /.login-logo -->
  
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="../../index2.html" class="h1"><b>Form</b> Registrasi</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Daftar untuk memulai aplikasi</p>

      <form action="/register" method="post">
        @csrf

        <div class="form-floating mb-3">
          <input type="text" class="form-control" id="name"  name="name" placeholder="Name" required value="{{ old('name') }}">
          <div class="input-group-append">
            
          </div>
          @error('name')
          <div class="text-danger" mb-3 >{{ $message }}</div>
          @enderror          
        </div>
        
        <div class="form-floating mb-3">
          <input type="text" class="form-control" id="username" name="username" placeholder="User Name" required value="{{ old('username') }}">
          <div class="input-group-append">
            
          </div>
          @error('username')
          <div class="text-danger" mb-3 >{{ $message }}</div>
          @enderror
        </div>

        <div class="form-floating mb-3">
          <input type="email" class="form-control" name="email" id="email" placeholder="Email" required value="{{ old('email') }}">
          <div class="input-group-append">
            
          </div>
          @error('email')
          <div class="text-danger align-top" mb-3 >{{ $message }}</div>
          @enderror
        </div>

        <div class="form-floating mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password" required >
          <div class="input-group-append">
            
          </div>
          @error('password')
          <div class="text-danger" mb-3 >{{ $message }}</div>
          @enderror
        </div>
        
          <!-- /.col -->
          <div class="col">
            <button type="submit" class="btn btn-primary btn-block">Daftar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      

    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

@endsection

