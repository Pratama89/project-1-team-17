@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
Input Tabel Likes di Media Sosial

@endsection

@section('content')

<form action="/likes" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        
        <label>User yang suka: </label>
        <input type="number" name="Userlikemu"><br>

            @error('Userlikemu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror



 
                <label>Postingan yang disukai:</label><br>
                    <select name="Postinganlikemu" class="form-control" id="">
                        <option value="">----Pilih Postingan yang disukai-----</option>

                        @foreach ($postslikes as $item)
                            <option value="{{$item->id}}">{{$item->isiposts}}</option>
                    
                        @endforeach
                    </select>

            @error('Postinganlikemu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror 

            <label>Jumlah Bintang: </label>
            <input type="number" name="likebintangmu"><br>
    
                @error('likebintangmu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror


            {{-- <label>User yang menyukai:</label><br>
                <select name="Userlikemu" class="form-control" id="">
                    <option value="">----Pilih User yang menyukai-----</option>

                     @foreach ($userlikes as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                
                    @endforeach
                </select>

            @error('Userlikemu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror 

  --}}

        

    </div>


   
    <button type="submit" class="btn btn-primary">Submit</button>
</form>  

@endsection