@extends('layouts/master')
@push('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endpush
@section('judul')
Input Tabel Posting di Media Sosial

@endsection

@section('content')

<form action="/posts" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Tanggal Post: </label>
        <input type="date" name="Tanggalmu"><br>

            @error('Tanggalmu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        
        <label>Isi Postingan:</label><br>
        <textarea name="Isipostsmu" cols="30"rows="5"></textarea><br>

            @error('Isipostsmu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

 
            <label>Kategori Berita:</label><br>
                <select name="kategori_idmu" class="form-control" id="">
                    <option value="">----Pilih Kategori Posting-----</option>

                     @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{$item->jenis}}</option>
                
                    @endforeach
                </select>

            @error('kategori_idmu')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror 


            
            <div class="form-group">
                <label> Thumbnail Foto Postingan </label>
                <input type="file" name="Fotomu" class="form-control">
            </div>
                @error('Fotomu')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
 

        

    </div>


   
    <button type="submit" class="btn btn-primary">Submit</button>
</form>  

@endsection