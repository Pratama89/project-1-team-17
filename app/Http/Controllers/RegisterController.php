<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.index');
    }

    public function store(Request $request)
    {
        $vallidatedData = $request->validate([
            'name' => 'required|max:255',
            'username' => ['required', 'min:3', 'max:255', 'unique:users'],
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5|max:255'
        ]);

        // $vallidatedData['password'] = bcrypt($vallidatedData['password']);
        $vallidatedData['password'] = Hash::make($vallidatedData['password']);

        User::create($vallidatedData);

        // $request->session()->flash('success', 'Anda berhasil Daftar! Silahkan Login');

        return redirect('/login')->with('success', 'Anda berhasil Daftar! Silahkan Login');
    }
}
