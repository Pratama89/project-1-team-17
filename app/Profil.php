<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = "profil";
    protected $fillable = ["umur","alamat ","bio","user_id"]; 

   
   //Tambahan relational ono to one disini
    public function user()   
    {
        return $this->belongsTo('App\User');   
    }
}


