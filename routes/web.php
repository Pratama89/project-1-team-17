<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});



Route::get('/profil', 'ProfilController@show');

Route::get('/login', 'LoginController@index')->name('login')->middleware('guest');
Route::post('/login', 'LoginController@authenticate');
Route::post('/logout', 'LoginController@logout');

Route::get('/register', 'RegisterController@index')->middleware('guest');
Route::post('/register', 'RegisterController@store');

Route::get('/dashboard', function () {
    return view('dashboard.index');
})->middleware('auth');


//Project -1 Group 17 CRUD dengan Eloquent ORM 
Route::resource('kategori', 'KategoriController');
Route::resource('posts', 'PostsController');
Route::resource('likes', 'LikesController');
Route::resource('profil', 'ProfilController');
